/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printUserInfo(){
	let fullName = prompt("Enter your Fullname:");
	let age = prompt("Enter your Age");
	let location = prompt("Enter your Location");

	console.log("Hello, " + fullName);
	console.log("Your are " + age + " years old.");
	console.log("You live in " + location);
}

printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function favoriteBands(){
	let favBand = ["The Beatles", "Metallica", "Eagles", "L'arc~en~Ciel", "Eraserheads"];
	let bandNum = 1;
	for (let b = 0; b <= 4; b++) {
 	console.log( bandNum + ". "  + favBand[b])
 	bandNum++;
}


}

favoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function movieRating() {
	let favMovies = ["The Godfather", "The Godfather, Part II", "Shawshank Redemption", "To kill A Mockingbird", "Psycho"];
	let rating = [97, 96, 91, 93, 96];
	let movieNum = 1;

	for (let m = 0; m <= 4; m++) {
 	console.log( movieNum + ". "  + favMovies[m]);
 	movieNum++;
 	console.log("Rotten Tomatoes Rating:"+ rating[m] + "%");
	}

	
}

movieRating();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();
/*
console.log(friend1);
console.log(friend2);
*/